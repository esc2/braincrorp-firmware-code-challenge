This is the code submission for Elliot Couvignou

:::WINDOWS:::
to run this progrom all you need to do is:

0. Open cmd change to this directory
1. type **gcc Distinguish.c** to compile
2. type **a.exe** to run the program
3. enter size of packet
4. enter each index (0x__) separated by enter presses
5. check my result :)



:::FOR LINUX:::
To run this program all you need to do is:

0. Open a terminal change to this directory
1. type **make** to compile (Note: uses gcc)
2. type **./test** to begin
3. enter size of packet
4. enter each index (0x__) separated by enter presses
5. check my result :)

if for some reason things dont work try **/make clean ** and repeat the steps.


Functions I wrote and their purpose:

- **int main(void)** : main loop that gathers user inputs as packet data to be
                  distinguished. This function calls Distinguish(uint8_t *data)
                  and then checks wether this function was successful at reading
                  the data.

- **int Distinguish(uint8_t *data)** : Takes a pointer to the data packet and then 
                  looks into the data to find the message_type, length, and ID.
                  Then check the message type to be Display (x34), motor 
                  message (x80) or neither. If the message type is known then 
                  the message_data is created following the appropriate data 
                  structure depending on message_type. Then call the correct
                  function inputting the message_data. 
                
                returns: 0 if the packet was successfully read
                         -1 if the packet has invalid data type or message_type


- **void display_message(char* message, uint16_t message_length)** :
                  This function first prompts to the user that this specific
                  function was called. Then displays the message in ASCII by 
                  looping through the char array/string.

- **void update_motor(int32_t forwardback, int32_t leftright)** :
                  This function first prompts the user that this specific 
                  function was called. Then it displays the forward_back and 
                  left_right data as a signed integer.





