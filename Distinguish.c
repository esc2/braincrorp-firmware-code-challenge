#include <stdio.h>
#include <inttypes.h>
#include <string.h>
#include <stdint.h>     
#ifndef  SCNu8
#define  SCNu8 "hhu"
#endif
#include "Distinguish.h"

int main(void){
	
	//starting off with actually getting the data and then inputting it
	//into the function ( void distinguish() ) that does the real work

	printf("enter size of packet \n");
	int size;
 
	scanf("%d", &size);
	int test;
	uint8_t scanned [size];
	int i = 0;	

	while(i<size){
		printf("Enter the %dth index of packet data \n", i);
		scanf("%8x" , &test);	
		scanned[i] = (unsigned char)test;
		i++;
	}
        printf("\n\n");	
	Distinguish(scanned); 	//pass reference into real function
	
	
	return 0; //done
}


//	The only real stuff that happens. First I separate the data into respective variables, then
//identify the message_type either as display motor or neither (fail). Create input data from rest to 
//be inputted and call correct function.
//
//Input:  Array pointer to unsigned bytes (uint8_t *array)
//Output: Prints an error statement IF the message type isn't recognized


void Distinguish(uint8_t *data){
	
	uint8_t packet_id = *data;
	uint8_t message_type = *(data+1);
	uint16_t message_length = *(data+2);     		// grouping useful data 
	
	if(message_type == 0x34){				 //messagetype = display
		char message_message [message_length];
		int i = 0;
		for(i = 0; i< message_length; i++){
			message_message[i] = *(data+i + 4);      //loop through data to creat string
		}
		display_message(message_message, message_length);
	} 
	else if(message_type == 0x80){				 //messagetpye = motor
		int32_t forwardback=0;
		int32_t leftright = 0;
		int i = 0;
		for(i=0; i<4; i++){ 			 //loop places data bytes into right
			forwardback += *(data+4+i);		 //spot
			leftright   += *(data+8+i);
			if(i!=3){
			leftright = leftright << 8;
			forwardback = forwardback << 8;          // left shift a byte except for last
			}
		}
		update_motor(forwardback, leftright);
	}
	else{
		printf(" unknown message_type \n");
		return;
	}

	return;
}


void display_message(char *message, uint16_t message_length){

	printf(" display_message() was called \n" );
	printf(" message: ");
	int i = 0;
	for(i=0; i<message_length; i++){
		printf("%c",message[i]);
	}
	return;
}

void update_motor( int32_t fowardback, int32_t leftright){

	printf(" update_motor() was called \n");
	printf(" Forward_back = %d \n", fowardback);
	printf(" Left_Right = %d \n", leftright);
	return;
}
