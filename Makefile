CC = gcc
CFLAGS = -I
LDFLAGS = -lm

.PHONY: all
all: test
test: Distinguish.o
	$(CC) -o test Distinguish.o


 
clean:
	rm -f *.o test
